﻿using RSIClientModule.App_Start;
using RSICommons.CommonModels;
using RSICommons.CommonModels.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RSIClientModule.Controllers
{
    [RequireHttps]
    public class ClientController : ApiController
    {
        public HttpResponseMessage GetTest()
        {
            var sth = new Person();
            sth.FirstName = "Malesza";
            sth.Secondname = "Sylwia";
            sth.PhoneNumber = "111111111";
            return Request.CreateResponse(HttpStatusCode.OK, new ApiResult { status = true, result = sth });
        }
    }
}
